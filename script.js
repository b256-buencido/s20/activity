//  Create a variable number that will store the value of the number provided by the user via the prompt.
let number = parseInt(prompt("Enter a number:"));

console.log("The number you provided is", + number);
// Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
for (let i = number; i > 0; i--) {
    //  Create a condition that if the current value is less than or equal to 50, stop the loop.
    if (i <= 50) {
        console.log("The current value is at " + i + ". Terminating the loop");
        break;
    }
  
    // Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
    if (i % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number");
        continue;
    }
  
    // Create another condition that if the current value is divisible by 5, print the number.
    if (i % 5 === 0) {
        console.log(i);
    }
}

// Create a variable that will contain the string supercalifragilisticexpialidocious.
let string = "supercalifragilisticexpialidocious";

console.log(string);

// Create another variable that will store the consonants from the string.
let consonants = "";

// Create another for Loop that will iterate through the individual letters of the string based on its length.
for (let i = 0; i < string.length; i++) {
    // Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
    if (string[i] === "a" || string[i] === "e" || string[i] === "i" || string[i] === "o" || string[i] === "u") {
        continue;
    }
    // Create an else statement that will add the letter to the second variable.
    else{
        consonants += string[i];
    }
}

console.log(consonants);
